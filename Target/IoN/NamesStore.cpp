#include <windows.h>

#include "PatternFinder.hpp"
#include "NamesStore.hpp"

#include "EngineClasses.hpp"

class FNameEntry
{
public:
	__int32 Index;
	char pad_0x0004[0x4];
	FNameEntry* HashNext;
	union
	{
		char AnsiName[1024];
		wchar_t WideName[1024];
	};

	const char* GetName() const
	{
		return AnsiName;
	}
};

template<typename ElementType, int32_t MaxTotalElements, int32_t ElementsPerChunk>
class TStaticIndirectArrayThreadSafeRead
{
public:
	int32_t Num() const
	{
		return NumElements;
	}

	bool IsValidIndex(int32_t index) const
	{
		return index >= 0 && index < Num() && GetById(index) != nullptr;
	}

	ElementType const* const& GetById(int32_t index) const
	{
		return *GetItemPtr(index);
	}

private:
	ElementType const* const* GetItemPtr(int32_t Index) const
	{
		int32_t ChunkIndex = Index / ElementsPerChunk;
		int32_t WithinChunkIndex = Index % ElementsPerChunk;
		ElementType** Chunk = Chunks[ChunkIndex];
		return Chunk + WithinChunkIndex;
	}

	enum
	{
		ChunkTableSize = (MaxTotalElements + ElementsPerChunk - 1) / ElementsPerChunk
	};

	ElementType** Chunks[ChunkTableSize];
	__int32 NumElements;
	__int32 NumChunks;
};

using TNameEntryArray = TStaticIndirectArrayThreadSafeRead<FNameEntry, 2 * 1024 * 1024, 0x4000>;

TNameEntryArray* GlobalNames = nullptr;
using GetNamesFn = TNameEntryArray * (__fastcall*)(__int64, __int64);
GetNamesFn GetNames;
bool NamesStore::Initialize()
{
	auto handle = reinterpret_cast<std::uintptr_t>(GetModuleHandle(NULL));
	//                                                                                          48  8B  1D ? ? ? ? 48 85 DB 75 35
	//0x351BB80
	auto address = FindPattern(GetModuleHandle(NULL), 
		reinterpret_cast<const unsigned char*>("\xE8\x00\x00\x00\x00\x45\x8B\x17"), 
		"x????xxx");
	if (address == -1)
	{
		return false;
	}
	auto offset = *reinterpret_cast<uint32_t*>(address + 1);
	GetNames = (GetNamesFn)(address + 5 + offset);
	GlobalNames = (GetNames(0,0));//reinterpret_cast<decltype(GlobalNames)>(*reinterpret_cast<uintptr_t*>(address + 7 + offset));
	printf(" \r\n Global Names: %p Count: %d", GlobalNames, GlobalNames->Num());
//	system("pause");
	/*
	
	printf(" \r\n Name: %s", GlobalNames->GetById(static_cast<int32_t>(1))->GetName());
	printf(" \r\n Name: %s", GlobalNames->GetById(static_cast<int32_t>(2))->GetName());
	printf(" \r\n Name: %s", GlobalNames->GetById(static_cast<int32_t>(3))->GetName());
	printf(" \r\n Name: %s", GlobalNames->GetById(static_cast<int32_t>(4))->GetName());
	printf(" \r\n Name: %s", GlobalNames->GetById(static_cast<int32_t>(10))->GetName());
	//system("pause");
	*/
	return true;
}

void* NamesStore::GetAddress()
{
	return GlobalNames;
}

size_t NamesStore::GetNamesNum() const
{
	return GlobalNames->Num();
}

bool NamesStore::IsValid(size_t id) const
{
	return GlobalNames->IsValidIndex(static_cast<int32_t>(id));
}

std::string NamesStore::GetById(size_t id) const
{
	return GlobalNames->GetById(static_cast<int32_t>(id))->GetName();
}
