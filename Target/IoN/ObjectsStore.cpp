#include <windows.h>

#include "PatternFinder.hpp"
#include "ObjectsStore.hpp"

#include "EngineClasses.hpp"

class FUObjectItem
{
public:
	UObject* Object; //0x0000
	__int32 Flags; //0x0008
	__int32 ClusterIndex; //0x000C
	__int32 SerialNumber; //0x0010
};

class TUObjectArray
{
public:
	FUObjectItem* Objects; // 0x0010
//	char unknown[0x4];     // 0x0014
	int32_t MaxElements;   // 0x0018
	int32_t NumElements;   // 0x001C
};

class FUObjectArray
{
public:
	__int32 ObjFirstGCIndex; //0x0000
	__int32 ObjLastNonGCIndex; //0x0004
	__int32 MaxObjectsNotConsideredByGC; //0x0008
	__int32 OpenForDisregardForGC; //0x000C

	TUObjectArray ObjObjects; //0x0010
};

FUObjectArray* GlobalObjects = nullptr;

bool ObjectsStore::Initialize()
{
	// 3627A00
	auto handle = reinterpret_cast<std::uintptr_t>(GetModuleHandle(NULL));
	// GWORLD 0x37291C0    // 48 8B 1D ? ? ? ? 48 85 DB 74 3B
	auto address = FindPattern(GetModuleHandle(NULL), reinterpret_cast<const unsigned char*>("\x48\x8D\x0D\x00\x00\x00\x00\x89\x54\x24\x3C"), "xxx????xxxx");
	if (address == -1)
	{
		return false;
	}
	auto offset = *reinterpret_cast<uint32_t*>(address + 3);
	GlobalObjects = reinterpret_cast<decltype(GlobalObjects)>(address + 7 + offset);
	printf("\r\n GObjects_Encrypted_PTR = 0x%p, Objects count: %d \r\n\r\n\r\n", GlobalObjects, GlobalObjects->ObjObjects.NumElements);
	printf("\r\n UObject:   0x%02X", sizeof(UObject));
	printf("\r\n UField:    0x%02X", sizeof(UField));
	printf("\r\n UStruct:   0x%03X", sizeof(UStruct));
	printf("\r\n UProperty: 0x%03X", sizeof(UProperty));
	printf("\r\n UFunction: 0x%03X", sizeof(UFunction));
	//system("pause");
	return true;
}

void* ObjectsStore::GetAddress()
{
	return GlobalObjects;
}

size_t ObjectsStore::GetObjectsNum() const
{
	return GlobalObjects->ObjObjects.NumElements;
}

UEObject ObjectsStore::GetById(size_t id) const
{
	return GlobalObjects->ObjObjects.Objects[id].Object;
}
